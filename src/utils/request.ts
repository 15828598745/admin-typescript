import axios from 'axios'
import { Message } from 'element-ui'
import { AppModule } from '@/store/modules/app';
import { EErrCode } from './errCode';
import { UserModule } from '@/store/modules/user';
import router from '@/router';

const service = axios.create({
  baseURL: AppModule.baseApi,
  timeout: 5000,
  withCredentials: true
})

service.interceptors.request.use(
  async (config) => {
    if (AppModule.baseApi === '') {
      await AppModule.GetBaseApi();
    }
    config.baseURL = AppModule.baseApi;
    return config
  },
  () => {
    Promise.resolve({ code: EErrCode.UnKnown, msg: '请求失败' })
  }
)

service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (res.code == EErrCode.NoLogin) {
      Message.error('登陆已过期，请重新登陆')
      UserModule.ResetName();
      router.push('/login')
      return Promise.resolve({ code: EErrCode.UnKnown, msg: '请求失败' })
    } else {
      return response.data
    }
  },
  () => {
    return Promise.resolve({ code: EErrCode.UnKnown, msg: '网络错误' })
  }
)

export default service
