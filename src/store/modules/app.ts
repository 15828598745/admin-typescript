import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators'
import store from '@/store'
import axios from 'axios'

export enum DeviceType {
  Mobile,
  Desktop
}

export interface IAppState {
  device: DeviceType
  sidebar: {
    opened: boolean
    withoutAnimation: boolean
  }
  size: string
}

@Module({ dynamic: true, store, name: 'app' })
class App extends VuexModule implements IAppState {
  sidebar = {
    opened: true,
    withoutAnimation: false
  }

  device = DeviceType.Desktop
  size = 'mini'
  baseApi = ''

  @Mutation
  TOGGLE_SIDEBAR(withoutAnimation: boolean) {
    this.sidebar.opened = !this.sidebar.opened
    this.sidebar.withoutAnimation = withoutAnimation
  }

  @Mutation
  CLOSE_SIDEBAR(withoutAnimation: boolean) {
    this.sidebar.opened = false
    this.sidebar.withoutAnimation = withoutAnimation
  }

  @Mutation
  TOGGLE_DEVICE(device: DeviceType) {
    this.device = device
  }

  @Mutation
  SET_SIZE(size: string) {
    this.size = size
  }

  @Mutation
  SET_BASEAPI(baseApi: string) {
    this.baseApi = baseApi
  }

  @Action
  ToggleSideBar(withoutAnimation: boolean) {
    this.TOGGLE_SIDEBAR(withoutAnimation)
  }

  @Action
  CloseSideBar(withoutAnimation: boolean) {
    this.CLOSE_SIDEBAR(withoutAnimation)
  }

  @Action
  ToggleDevice(device: DeviceType) {
    this.TOGGLE_DEVICE(device)
  }

  @Action
  SetSize(size: string) {
    this.SET_SIZE(size)
  }

  @Action
  SetBaseApi(baseApi: string) {
    this.SET_BASEAPI(baseApi)
  }
  @Action
  async GetBaseApi() {
    let config = await axios.get('./config.json');
    if (config.status === 200) {
      let { baseApi } = config.data;
      if (baseApi) {
        this.SET_BASEAPI(baseApi)
      } else {
        console.error('base_api配置无效！');
      }
    } else {
      console.error('获取配置失败！');
    }
  }
}

export const AppModule = getModule(App)
